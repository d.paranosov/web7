
$(document).ready(function(){
    console.log( "ready!" );
    $('.slider').slick({
        arrows: true,
        dots: true,
        slidesToShow: 4,
        slidesToScroll: 4,
        speed: 600,
        infinite: false,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                }
            }] 
    });
});